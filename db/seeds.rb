# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

USERS = [
  {
    email: 'user@worldofmind.org',
    username: 'user',
    password: 'user42'
  },
  {
    email: 'user2@worldofmind.org',
    username: 'user2',
    password: 'user42'
  }
]

ADMINS = [
  {
    email: 'admin@worldofmind.org',
    username: 'admin',
    password: 'admin42'
  },
  {
    email: 'admin2@worldofmind.org',
    username: 'admin2',
    password: 'admin42'
  }
]

def create_users
  USERS.each do |user_params|
    user = User.create!(user_params)
    user.confirm
    pp "user - #{user.username} created."
  end
end

def create_admins
  ADMINS.each do |admin_params|
    admin = User.create!(admin_params)
    admin.admin = true
    admin.confirm
    pp "admin - #{admin.username} created."
  end
end

create_users
create_admins
