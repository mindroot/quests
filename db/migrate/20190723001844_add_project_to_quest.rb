class AddProjectToQuest < ActiveRecord::Migration[6.0]
  def change
    add_reference :quests, :project, foreign_key: true
  end
end
