class AddSolutionsToQuest < ActiveRecord::Migration[5.2]
  def change
    add_reference :quests, :solution, foreign_key: true
  end
end
