class CreateQuests < ActiveRecord::Migration[5.2]
  def change
    create_table :quests do |t|
      t.string :title
      t.string :slug
      t.integer :status

      t.string :seed
      t.string :latest_address
      t.integer :balance

      t.references :creator
      t.references :solver

      t.datetime :payed_at
      t.integer :payed_amount

      t.timestamps
    end

    add_foreign_key :quests, :users, column: :creator_id, primary_key: :id
    add_foreign_key :quests, :users, column: :solver_id, primary_key: :id
  end
end
