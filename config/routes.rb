require 'sidekiq/web'

Rails.application.routes.draw do
  resources :projects
  mount ActionCable.server => '/cable'
  resources :quests do
    resources :solutions do
       post 'payout'
     end
  end
  get :next_quest, to: 'quests#next'

  namespace :admin do
      resources :users
      resources :announcements
      resources :notifications
      resources :services

      root to: "users#index"
    end
  get '/privacy', to: 'home#privacy'
  get '/get_started', to: 'home#get_started'
  get '/creators', to: 'home#creators'
  get '/roadmap', to: 'home#roadmap'

  resources :notifications, only: [:index]
  resources :announcements, only: [:index]
  authenticate :user, lambda { |u| u.admin? } do
    mount Sidekiq::Web => '/sidekiq'
  end

  devise_for :users, controllers: { omniauth_callbacks: "users/omniauth_callbacks" }
  root to: 'home#index'
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html

  ### API ROUTES
  namespace :api, constraints: { format: 'json' } do
    namespace :v1 do
      post '/login', to: 'auth#login'
      resources :quests
    end
  end

end
