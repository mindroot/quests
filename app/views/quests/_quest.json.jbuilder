json.extract! quest, :title, :latest_address, :balance
json.url quest_url(quest, format: :json)
json.link quest_url(quest)
