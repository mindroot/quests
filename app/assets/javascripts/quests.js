$(document).on('turbolinks:load', function() {
  $("tr[data-link]").click(function() {
    window.location = $(this).data("link")
  })

  // Button for copy the iota address for a quest
  $(".buttonCopyAddressk").on("click", function(event) {
    var address = $(this).data('address');
    copyToClipboard(address)
    successAnimation(event.target)
  });
  // Button for copy the link to a solution to the clipboard
  $(".buttoncopySolutionLink").on("click", function(event) {
    console.log(window.location);
    var url =  window.location.protocol + '//' + window.location.host + window.location.pathname
    var id = event.target.parentElement.parentElement.id
    copyToClipboard(url + "#" + id)
    successAnimation(event.target)
  });
  // Checks the solution in the url and makes it visible with some color
  var solution_id = window.location.hash.substr(1);
  if(solution_id) {
    var solution = $('#' + solution_id);
    solution.addClass('selected')
  }
});
