class SolutionsController < ApplicationController
  before_action :authenticate_user!, except: :create

  before_action :set_quest

  def create
    @solution =  Solution.new(solution_params)
    @solution.quest = @quest
    @solution.creator = current_user if params[:user] == "true"

    if @solution.save
      redirect_to quest_path(@quest, anchor: "solution-#{@quest.solutions.count}"), notice: 'Solution was successfully created.'
    else
      render "quests/show"
    end
  end

  def index
    @message = Solution.new
    render "quests/show"
  end

  def payout
    return permission_denied unless current_user == @quest.creator
    solution = @quest.solutions.find(params[:solution_id])
    respond_to do |format|
      if @quest.payout(solution)
        format.html { redirect_to @quest, notice: 'Payout was a success.' }
        format.json { render :show, status: :created, location: @quest }
      else
        format.html { redirect_to @quest, notice: 'Something went wrong!' }
        format.json { render json: @quest.errors, status: :unprocessable_entity }
      end
    end
  end


  private

    def set_quest
      @quest = Quest.find(params[:quest_id])
    end

    def solution_params
      params.require(:solution).permit(:text, :address)
    end

end
