class Api::V1::QuestsController < ApiController
  skip_before_action :verify_authenticity_token
  before_action :authorize_request, only: [:create]
  before_action :set_quest, only: [:show]
  
  # GET /api/v1/quests
  def index
    @quests = Quest.all
    respond_to do |format|
      if @quests
        format.json { render "quests/index", quests: @quests, status: :ok}
      else
        format.json { render json: @quests, status: :empty }
      end
    end
  end

  # POST /api/v1/quests
  def create
  @quest = current_user.created_quests.new(quest_params)
    respond_to do |format|
      if @quest.save
        format.json { render "quests/show", status: :created, location: @quest }
      else
        format.json { render json: @quest.errors, status: :unprocessable_entity }
      end
    end
  end

  # GET /api/v1/quests/:slug
  def show
    respond_to do |format|
      if @quest
        format.json { render "quests/show", quest: @quest, status: :ok}
      else
        format.json { render json: @quest, status: :not_found }
      end
    end
  end

private
  # Use callbacks to share common setup or constraints between actions.
  def set_quest
    @quest = Quest.find_by(slug: params[:id])
  end

  # Never trust parameters from the scary internet, only allow the white list through.
  def quest_params
    params.require(:quest).permit(:title)
  end

end