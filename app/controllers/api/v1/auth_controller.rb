class Api::V1::AuthController < ApplicationController
   skip_before_action :verify_authenticity_token, :only => [:login]

  def login
    auth_object = Authentication.new(login_params)
    if auth_object.authenticate
      render json: {
        message: "Login successful!", token: auth_object.generate_token, user: auth_object.current_user.to_json(only: [:email, :username]) }, status: :ok
    else
      render json: {
        message: "Incorrect username/password combination"}, status: :unauthorized
    end
  end
  private
  def login_params
    params.permit(:email, :password)
  end
end
