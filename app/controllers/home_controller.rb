class HomeController < ApplicationController
  def index
     @hero_quest = Quest.all.where(status: :active).first
     @open_quest_count = Quest.all.where(status: :active).count
     @sovled_quest_coun = Quest.all.where(status: :solved).count
  end

  def get_started
  end

  def creators
  end

  def roadmap
  end

  def privacy
  end
end
