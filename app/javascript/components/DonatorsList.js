import React from "react"
import PropTypes from "prop-types"

import { composeAPI } from '@iota/core'
// converter module required to convert ascii msg to trytes
const converter = require('@iota/converter')
const extract = require('@iota/extract-json')

const iota = composeAPI({
  provider: 'https://nodes.devnet.iota.org'
})


class DonatorsList extends React.Component {

  constructor(props) {
    super(props);
    // Don't call this.setState() here!
    this.state = {
      donators: []
    };
    this.getBalance(props.address)
  }

  async componentDidMount() {

  }


  getBalance(address) {

    iota.findTransactionObjects({ addresses: [this.props.address] })
       .then(transactions => {
          console.log("transactions", transactions);
          transactions.forEach(function(transaction){
            if(transaction.value <= 0) {
              transactions.pop(transaction)
            }
          })
          this.setState({
              donators: transactions
          })
       })
       .catch(err => {
           // ...
           console.log("err", err);

       })
    }

    getDonatorName(trytes_message) {
      console.log("trytes_message", trytes_message);
      var message = converter.trytesToAscii(trytes_message + '9')
      console.log("message:", message);
      return message
    }

    getDonatorTime(timestamp) {
      var date = new Date(timestamp * 1000)
      // Hours part from the timestamp
      var hours = date.getHours();
      // Minutes part from the timestamp
      var minutes = "0" + date.getMinutes();
      // Seconds part from the timestamp
      var seconds = "0" + date.getSeconds();

      var formattedTime = hours + ':' + minutes.substr(-2) + ':' + seconds.substr(-2)
      return formattedTime
    }



  render () {

    const liStyle = {
      listStyleType: 'none',
    };
    const donatorsList = this.state.donators.map((transaction, index) => {
      return <li key="index" style={liStyle}>{transaction.value} IOTA donated by <strong>{this.getDonatorName(transaction.signatureMessageFragment)}</strong> at { this.getDonatorTime(transaction.timestamp) } </li>
    }
    );
    return (
      <React.Fragment>
        <div className="card text-center">
          <div className="card-body">
            <h4 className="card-title">List of Donators</h4>
            <h6 className="card-title">Actual Donators: {this.state.donators.length}</h6>
            <ul>
            {donatorsList}
            </ul>
          </div>
        </div>
      </React.Fragment>
    );
  }
}

DonatorsList.propTypes = {
  address: PropTypes.string
};
export default DonatorsList
