class Solution < ApplicationRecord
  require 'iota'

  belongs_to :quest
  belongs_to :creator, class_name: 'User', foreign_key: 'user_id', optional: true

  validates_length_of :text, :minimum => 2, :maximum => 3000
  validate :valid_iota_address


  def valid_iota_address
    utils = IOTA::Utils::InputValidator.new
    errors.add(:address, "Not a valid iota address.") unless utils.isAddress(address)
  end
end
