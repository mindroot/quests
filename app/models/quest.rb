class Quest < ApplicationRecord
  require 'iota'

  before_create :create_address

  extend FriendlyId
  friendly_id :title, use: [:slugged, :finders]

  enum status: [:active, :payout_proccessed, :solved]

  belongs_to :project, optional: true

  belongs_to :creator, class_name: 'User'
  belongs_to :solver, class_name: 'User', optional: true

  has_many :solutions
  has_one :solution

  validates :title, :slug, presence: true
  validates :title, length: { minimum: 5, maximum: 81 }

  def payout message

    utils = IOTA::Utils::InputValidator.new
    return false unless utils.isAddress(message.address)


    self.solver = solution.creator if solution.creator
    self.solution = solution
    SolutionPayoutJob.perform_later(self, solution)
    self.payout_proccessed!

   return true
  end

  private
  def create_address
    # Create client directly with provider
    client = IOTA::Client.new(provider: 'https://nodes.devnet.iota.org')

    # now you can start using all of the functions
    status, data = client.api.getNodeInfo

    ## create and save seed
    self.seed = (1..81).map{'ABCDEFGHIJKLMNOPQRSTUVWXYZ9'[SecureRandom.random_number(27)]}.join('')

    ## create and save latest address
    account = IOTA::Models::Account.new(client, self.seed)
    account.getAccountDetails({})

    self.latest_address = account.latestAddress
    self.status = :active
  end
end
