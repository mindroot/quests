class User < ApplicationRecord
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise  :masqueradable, :database_authenticatable, :registerable,
          :recoverable, :rememberable, :validatable, :omniauthable,
          :trackable, :lockable, :confirmable, :timeoutable

  has_person_name

  has_many :notifications, foreign_key: :recipient_id
  has_many :services

  has_many :created_quests, class_name: 'Quest', foreign_key: 'creator_id'
  has_many :solved_quests,  class_name: 'Quest', foreign_key: 'solver_id'

  extend FriendlyId
  friendly_id :username, use: [:slugged, :finders]

  # VALIDATIONS
  validates :username, length: { minimum: 1, maximum: 100 }

  has_many :projects
end
