FactoryBot.define do
  factory :quest do
    title { "Test title" }
    association :creator, factory: :user
    association :solver, factory: :user
  end
end
