require 'support/acceptance_helper'

resource "Quests" do
  header "Accept", "application/json"
  header "Content-Type", "application/json"

  explanation "Quests are top-level business objects"

  let(:quest) { FactoryBot.create(:quest) }

  get "/api/v1/quests" do
    authentication :apiKey, "API_TOKEN", :name => "AUTH_TOKEN"
    #parameter :page, "Get a list of all quests", with_example: true

    #let(:page) { 1 }

    before do
      2.times do |i|
        Quest.create(:title => "Quest #{i}")
      end
    end

    example_request "Getting a list of quests" do
      expect(response_body).to eq(Quest.all.to_json)
      expect(status).to eq(200)
    end
  end

  head "/api/v1/quests" do
    authentication :apiKey, "API_TOKEN", :name => "AUTH_TOKEN"

    example_request "Getting the headers" do
      expect(response_headers["Cache-Control"]).to eq("max-age=0, private, must-revalidate")
    end
  end

  post "/api/v1/quests" do
    with_options :scope => :quest, :with_example => true do
      parameter :title, "Title of quest", :required => true
    end

    with_options :scope => :quest do
      response_field :title, "Title of quest", :type => :string
    end

    let(:title) { "Quest 1" }

    let(:raw_post) { params.to_json }

    @user = FactoryBot.create(:user)
    @token = JsonWebToken.encode(user_id: @user.id)
    header "Authorization", @token


    example_request "Creating an quest" do
      explanation "First, create an quest, then make a later request to get it back"

      quest = JSON.parse(response_body)
      expect(quest.except("id", "created_at", "updated_at", "balance", "latest_address", "url", "link")).to eq({
        "title" => title
      })
      expect(status).to eq(201)

      client.get(URI.parse(response_headers["location"]).path, {}, headers)
      expect(status).to eq(200)
    end
  end

  get "/api/v1/quests/:slug" do
    context "when slug is valid" do
      let(:slug) { quest.slug }

      example_request "Getting a specific quest via slug" do
        expect(response_body).to  include(quest.title, quest.latest_address)
        expect(status).to eq(200)
      end
    end

    context "when slug is invalid" do
      let(:slug) { "a" }

      example_request "Getting an error" do
        expect(response_body).to eq("null")
        expect(status).to eq 404
      end
    end
  end

  put "/api/v1/quests/:id" do
    with_options :scope => :quest, with_example: true do
      parameter :title, "title of quest"
    end

    let(:id) { quest.id }
    let(:title) { "Updated Name" }

    let(:raw_post) { params.to_json }

    xexample "Updating an quest" do
      do_request
      expect(status).to eq(204)
    end
  end

  delete "/api/v1/quests/:id" do
    let(:id) { quest.id }

    xexample "Deleting an quest" do
      do_request
      expect(status).to eq(204)
    end
  end
end